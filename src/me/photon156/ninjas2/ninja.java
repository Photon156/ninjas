package me.photon156.ninjas2;

/*
 * [Purpose]: Sets values of variables; easier way to set the correct armor for the player
 * Variables are the player's armor/weapons/etc. from the config
 */

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ninja {
	private final Main plugin;
	public ninja(Main plugin){
		this.plugin=plugin;
	}
	private ItemStack hat;
	private ItemStack vest;
	private ItemStack pants;
	private ItemStack boots;
	private int bal;
	private ItemStack sword;
	private int shurikens;
	private int grenades;
	private boolean vip;
	private int kills;
	public void pop(Player target){ //See above for explanation
		//**See onJoin calling setup*/
		hat=plugin.getConfig().getItemStack(target.getUniqueId()+".hat");
		vest=plugin.getConfig().getItemStack(target.getUniqueId()+".vest");
		pants=plugin.getConfig().getItemStack(target.getUniqueId()+".pants");
		boots=plugin.getConfig().getItemStack(target.getUniqueId()+".boots");
		bal=plugin.getConfig().getInt(target.getUniqueId()+".bal");
		sword=plugin.getConfig().getItemStack(target.getUniqueId()+".sword");
		shurikens=plugin.getConfig().getInt(target.getUniqueId()+".grenades");
		grenades=plugin.getConfig().getInt(target.getUniqueId()+".shurikens");
		vip=plugin.getConfig().getBoolean(target.getUniqueId()+".vip");
		kills=plugin.getConfig().getInt(target.getUniqueId()+".kills");
	}
	public ItemStack getHat(){return hat;}
	public ItemStack getVest(){return vest;}
	public ItemStack getPants(){return pants;}
	public ItemStack getBoots(){return boots;}
	public int getBal(){return bal;}
	public ItemStack getSword(){return sword;}
	public int getShurikens(){return shurikens;}
	public int getGrenades(){return grenades;}
	public boolean getVip(){return vip;}
	public int getKills(){return kills;}
	
}
