package me.photon156.ninjas2;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

/*
 * [Purpose] of class: track users in each world. When users want to join a game,
 * it sends them to a game that has <10 players in it.
 * Handles fixing player inventories when joining a queue & the game itself; relies
 * upon separate bean class for inv items
 */

public class worldhandler {
private final Main plugin;
private final Main Main;
public worldhandler(Main plugin, Main main){
	this.plugin=plugin;
	this.Main=main;
}
Player target;
int size=4; //[Debug] Changes game size
String prefix="&8[&aNinjas&8] &7"; //Ninjas plugin prefix
public void join(Player target,int game){ //called when a player wants to join a game
	if(Bukkit.getServer().getPlayer(target.getUniqueId()) != null){ //If a valid player is passed in
		this.target=target;
		ninja ninja=new ninja(plugin);
		if(game>0){ //Game specified; check: <=[size] players?
			if(game==1){
				if(Main.game1.size()+1<=size){
					Main.game1.put(target.getUniqueId(),null); //Adds player to player list
					reJoin(target,1);
					send(prefix+"You're joining game #1");
					send(prefix+"There are currently "+Main.game1.size()+"/"+size+" players in the game.");
					//Inventory editing -------------------
					ninja.pop(target); //Refreshes variables for player
					target.getInventory().clear();
					target.getInventory().setHelmet(ninja.getHat());
					target.getInventory().setChestplate(ninja.getVest());
					target.getInventory().setLeggings(ninja.getPants());
					target.getInventory().setBoots(ninja.getBoots());
					target.getInventory().addItem(ninja.getSword());
					items items=new items(Main,target);
					target.getInventory().addItem(items.extras[1]);
					target.getInventory().addItem(items.extras[0]);
					worldspawns(target,1); //Spawns player @ random spawnpoint in arena (limited by config)
					//TODO Invulnerability timer??
				}else{
					send(prefix+"Sorry! That game is full right now!");
				}
			}else if(game==2){
				if(Main.game2.size()+1<=size){
					send(prefix+"You're joining game #2");
					Main.game2.put(target.getUniqueId(),null);
					reJoin(target,2);
					send(prefix+"There are currently "+Main.game2.size()+"/"+size+" players in the game.");
					//Inventory editing -------------------
					ninja.pop(target); //Refreshes variables for player
					target.getInventory().clear();
					target.getInventory().setHelmet(ninja.getHat());
					target.getInventory().setChestplate(ninja.getVest());
					target.getInventory().setLeggings(ninja.getPants());
					target.getInventory().setBoots(ninja.getBoots());
					target.getInventory().addItem(ninja.getSword());
					items items=new items(Main,target);
					target.getInventory().addItem(items.extras[1]);
					target.getInventory().addItem(items.extras[0]);
					worldspawns(target,1); //Spawns player @ random spawnpoint in arena (limited by config)
					//TODO Invulnerability timer??
				}else{
					send(prefix+"Sorry! That game is full right now!");
				}
			}else if(game==3){
				if(Main.game3.size()+1<=size){
					send(prefix+"You're joining game #3");
					Main.game3.put(target.getUniqueId(),null);
					reJoin(target,3);
					send(prefix+"There are currently "+Main.game3.size()+"/"+size+" players in the game.");
					//Inventory editing -------------------
					ninja.pop(target); //Refreshes variables for player
					target.getInventory().clear();
					target.getInventory().setHelmet(ninja.getHat());
					target.getInventory().setChestplate(ninja.getVest());
					target.getInventory().setLeggings(ninja.getPants());
					target.getInventory().setBoots(ninja.getBoots());
					target.getInventory().addItem(ninja.getSword());
					items items=new items(Main,target);
					target.getInventory().addItem(items.extras[1]);
					target.getInventory().addItem(items.extras[0]);
					worldspawns(target,1); //Spawns player @ random spawnpoint in arena (limited by config)
					//TODO Invulnerability timer??
				}else{
					send(prefix+"Sorry! That game is full right now!");
				}
			}else{
				send(prefix+"&cThere are only three games to choose from (1/2/3)!");
			}
			
		}else{ //No game specified; autopick
			if(Main.game1.size()+1<=size){
				Main.game1.put(target.getUniqueId(),null); //Adds player to player list
				reJoin(target,1);
				send(prefix+"You're joining game #1");
				send(prefix+"There are currently "+Main.game1.size()+"/"+size+" players in the game.");
				//Inventory editing -------------------
				ninja.pop(target); //Refreshes variables for player
				target.getInventory().clear();
				target.getInventory().setHelmet(ninja.getHat());
				target.getInventory().setChestplate(ninja.getVest());
				target.getInventory().setLeggings(ninja.getPants());
				target.getInventory().setBoots(ninja.getBoots());
				target.getInventory().addItem(ninja.getSword());
				items items=new items(Main,target);
				target.getInventory().addItem(items.extras[1]);
				target.getInventory().addItem(items.extras[0]);
				worldspawns(target,1); //Spawns player @ random spawnpoint in arena (limited by config)
				//TODO Invulnerability timer??
				
			}else if(Main.game2.size()+1<=size){
				send(prefix+"You're joining game #2");
				Main.game2.put(target.getUniqueId(),null);
				reJoin(target,2);
				send(prefix+"There are currently "+Main.game2.size()+"/"+size+" players in the game.");
				//Inventory editing -------------------
				ninja.pop(target); //Refreshes variables for player
				target.getInventory().clear();
				target.getInventory().setHelmet(ninja.getHat());
				target.getInventory().setChestplate(ninja.getVest());
				target.getInventory().setLeggings(ninja.getPants());
				target.getInventory().setBoots(ninja.getBoots());
				target.getInventory().addItem(ninja.getSword());
				items items=new items(Main,target);
				target.getInventory().addItem(items.extras[1]);
				target.getInventory().addItem(items.extras[0]);
				worldspawns(target,1); //Spawns player @ random spawnpoint in arena (limited by config)
				//TODO Invulnerability timer??
				
			}else if(Main.game3.size()+1<=size){
				send(prefix+"You're joining game #3");
				Main.game3.put(target.getUniqueId(),null);
				reJoin(target,3);
				send(prefix+"There are currently "+Main.game3.size()+"/"+size+" players in the game.");
				//Inventory editing -------------------
				ninja.pop(target); //Refreshes variables for player
				target.getInventory().clear();
				target.getInventory().setHelmet(ninja.getHat());
				target.getInventory().setChestplate(ninja.getVest());
				target.getInventory().setLeggings(ninja.getPants());
				target.getInventory().setBoots(ninja.getBoots());
				target.getInventory().addItem(ninja.getSword());
				items items=new items(Main,target);
				target.getInventory().addItem(items.extras[1]);
				target.getInventory().addItem(items.extras[0]);
				worldspawns(target,1); //Spawns player @ random spawnpoint in arena (limited by config)
				//TODO Invulnerability timer??
			}else{
				send(prefix+"Sorry! All games are full right now!");
			}
		}
	}
}
public void leave(Player target,boolean replace){ //Called when a player is supposed to leave a game for some reason
	this.target=target;
	if(Main.game1.containsKey(target.getUniqueId())){
		Main.game1.remove(target.getUniqueId());
	}
	if(Main.game2.containsKey(target.getUniqueId())){
		Main.game2.remove(target.getUniqueId());
	}
	if(Main.game3.containsKey(target.getUniqueId())){
		Main.game3.remove(target.getUniqueId());
	}
	if(replace==false){
		target.teleport(new Location(Bukkit.getServer().getWorld(plugin.getConfig().getString("lobby.world")),plugin.getConfig().getDoubleList("lobby.spawncoords").get(0),plugin.getConfig().getDoubleList("lobby.spawncoords").get(1),plugin.getConfig().getDoubleList("lobby.spawncoords").get(2)));
	}
	new BukkitRunnable(){
		public void run(){
			target.setFireTicks(0);
		}
	}.runTaskLater(plugin,10);
	target.setHealth(20);
	target.setFoodLevel(20);
	target.getInventory().clear();
	for(PotionEffect effect:target.getActivePotionEffects()){
		target.removePotionEffect(effect.getType());
	}
}
public boolean inGame(Player target){ //Checks if a player is in a game; boolean output
	if(Main.game1.containsKey(target.getUniqueId())){
		return true;
	}else if(Main.game2.containsKey(target.getUniqueId())){
		return true;
	}else if(Main.game3.containsKey(target.getUniqueId())){
		return true;
	}else{
		return false;
	}
}
public int currentGame(Player target){
	if(Main.game1.containsKey(target.getUniqueId())){
		return 1;
	}else if(Main.game2.containsKey(target.getUniqueId())){
		return 2;
	}else if(Main.game3.containsKey(target.getUniqueId())){
		return 3;
	}else{
		return 0;
	}
}
public void reJoin(Player target,int game){
	if(inGame(target)){
		if(!(currentGame(target)==game)){
			leave(target,true);
			join(target,game);
		}
	}
}
public final Location worldspawns(Player target,int game){ //Game passed in. Random int generated and it randomly chooses a spawnpoint
	double a=Math.random();
	a*=100;
	World world;
	double x;
	double y;
	double z;
	if(game==1){
		if(a>=0&&a<=33.3){
			world=Bukkit.getServer().getWorld(plugin.getConfig().getString("game1.world"));
			x=plugin.getConfig().getIntegerList("game1.spawn1coords").get(0);
			y=plugin.getConfig().getIntegerList("game1.spawn1coords").get(1);
			z=plugin.getConfig().getIntegerList("game1.spawn1coords").get(2);
			target.teleport(new Location(world,x,y,z));
		}else if(a>33.3&&a<=67){
			world=Bukkit.getServer().getWorld(plugin.getConfig().getString("game1.world"));
			x=plugin.getConfig().getIntegerList("game1.spawn2coords").get(0);
			y=plugin.getConfig().getIntegerList("game1.spawn2coords").get(1);
			z=plugin.getConfig().getIntegerList("game1.spawn2coords").get(2);
			target.teleport(new Location(world,x,y,z));
		}else if(a>67&&a<=100){
			world=Bukkit.getServer().getWorld(plugin.getConfig().getString("game1.world"));
			x=plugin.getConfig().getIntegerList("game1.spawn3coords").get(0);
			y=plugin.getConfig().getIntegerList("game1.spawn3coords").get(1);
			z=plugin.getConfig().getIntegerList("game1.spawn3coords").get(2);
			target.teleport(new Location(world,x,y,z));
		}
	}else if(game==2){
		if(a>=0&&a<=33.3){
			world=Bukkit.getServer().getWorld(plugin.getConfig().getString("game2.world"));
			x=plugin.getConfig().getIntegerList("game2.spawn1coords").get(0);
			y=plugin.getConfig().getIntegerList("game2.spawn1coords").get(1);
			z=plugin.getConfig().getIntegerList("game2.spawn1coords").get(2);
			target.teleport(new Location(world,x,y,z));
		}else if(a>33.3&&a<=67){
			world=Bukkit.getServer().getWorld(plugin.getConfig().getString("game2.world"));
			x=plugin.getConfig().getIntegerList("game2.spawn2coords").get(0);
			y=plugin.getConfig().getIntegerList("game2.spawn2coords").get(1);
			z=plugin.getConfig().getIntegerList("game2.spawn2coords").get(2);
			target.teleport(new Location(world,x,y,z));
		}else if(a>67&&a<=100){
			world=Bukkit.getServer().getWorld(plugin.getConfig().getString("game2.world"));
			x=plugin.getConfig().getIntegerList("game2.spawn3coords").get(0);
			y=plugin.getConfig().getIntegerList("game2.spawn3coords").get(1);
			z=plugin.getConfig().getIntegerList("game2.spawn3coords").get(2);
			target.teleport(new Location(world,x,y,z));
		}
	}else if(game==3){
		if(a>=0&&a<=33.3){
			world=Bukkit.getServer().getWorld(plugin.getConfig().getString("game3.world"));
			x=plugin.getConfig().getIntegerList("game3.spawn1coords").get(0);
			y=plugin.getConfig().getIntegerList("game3.spawn1coords").get(1);
			z=plugin.getConfig().getIntegerList("game3.spawn1coords").get(2);
			target.teleport(new Location(world,x,y,z));
		}else if(a>33.3&&a<=67){
			world=Bukkit.getServer().getWorld(plugin.getConfig().getString("game3.world"));
			x=plugin.getConfig().getIntegerList("game3.spawn2coords").get(0);
			y=plugin.getConfig().getIntegerList("game3.spawn2coords").get(1);
			z=plugin.getConfig().getIntegerList("game3.spawn2coords").get(2);
			target.teleport(new Location(world,x,y,z));
		}else if(a>67&&a<=100){
			world=Bukkit.getServer().getWorld(plugin.getConfig().getString("game3.world"));
			x=plugin.getConfig().getIntegerList("game3.spawn3coords").get(0);
			y=plugin.getConfig().getIntegerList("game3.spawn3coords").get(1);
			z=plugin.getConfig().getIntegerList("game3.spawn3coords").get(2);
			target.teleport(new Location(world,x,y,z));
		}
	}
	return null;
}
private final void send(String message){ //[Ease] Sends messages to players
	target.sendMessage(ChatColor.translateAlternateColorCodes('&',message)); //So we can use &'s for colors
}
}
