package me.photon156.ninjas2;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.math.NumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{ //TODO: death messages; scoreboard; game mechanics; perms; anticheating; /help;
	Map<UUID,Location> game1,game2,game3=new HashMap<UUID,Location>(); //Playerlist for game(1/2/3)
	Player target;
	String prefix="&8[&aNinjas&8] &7";
	Map<Player,String> inCombat=new ConcurrentHashMap<Player,String>();
	Map<Player,Thread> threads=new ConcurrentHashMap<Player,Thread>();
	@Override
	public void onEnable(){
		this.getConfig().options().copyDefaults(true); //Copy defaults
		new onJoin(this);
		new health(this,this);
		new EventListener(this,this);
		game1=new HashMap<UUID, Location>();
		game2=new HashMap<UUID, Location>();
		game3=new HashMap<UUID, Location>();
	}
	CommandSender sender;
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) { {
		if(sender!=null){
			this.sender=sender;
			target=(Player) sender;
			worldhandler worldhandler=new worldhandler(this,this);
			if(commandLabel.equalsIgnoreCase("ninjas")){
				target.teleport(new Location(Bukkit.getServer().getWorld(this.getConfig().getString("lobby.world")),this.getConfig().getDoubleList("lobby.spawncoords").get(0),this.getConfig().getDoubleList("lobby.spawncoords").get(1),this.getConfig().getDoubleList("lobby.spawncoords").get(2)));
			}else if(commandLabel.equalsIgnoreCase("join")){
				if(!worldhandler.inGame((Player)sender)){
					if(args==null||args.length==0){
						worldhandler.join((Player)sender,0);
					}else if(args.length==1){
						worldhandler.join((Player)sender,Integer.valueOf(args[0]));
					}else{
						a(prefix+"&cCorrect usage is: /join [game #]");
					}
				}else{
					a(prefix+"Nice try; you're already in a game! &9Did you mean &n/leave&9?");
				}
			}else if(commandLabel.equalsIgnoreCase("listGames")){
				if(game1.size()>=worldhandler.size){
					a(prefix+"Game 1: &c"+game1.size()+"&7/"+worldhandler.size);
				}else{
					a(prefix+"Game 1: "+game1.size()+"/"+worldhandler.size);
				}
				if(game2.size()>=worldhandler.size){
					a(prefix+"Game 2: &c"+game2.size()+"&7/"+worldhandler.size);
				}else{
					a(prefix+"Game 2: "+game2.size()+"/"+worldhandler.size);
				}
				if(game3.size()>=worldhandler.size){
					a(prefix+"Game 3: &c"+game3.size()+"&7/"+worldhandler.size);
				}else{
					a(prefix+"Game 3: "+game3.size()+"/"+worldhandler.size);
				}
			}else if(commandLabel.equalsIgnoreCase("balance")){
				ninja ninja=new ninja(this);
				ninja.pop((Player)sender);
				if(args.length==0){
					a(prefix+"Your current balance is "+ninja.getBal()+" coins.");
				}else{
					target=Bukkit.getServer().getPlayer(args[0]);
					if(!(target instanceof Player)){
						rec(prefix+"Offline or invalid player: "+args[0]);
					}else{
						ninja.pop(target);
						rec(prefix+target.getName()+"'s current balance is "+ninja.getBal()+" coins.");
					}
				}
			}else if(commandLabel.equalsIgnoreCase("coins")){
				balance balance=new balance(this,this);
				if(args.length<=2){
					a(prefix+"Correct usage: /coins <set/add/subtract> <player> <amt>");
				}else{
					target=Bukkit.getServer().getPlayer(args[1]);
					if(target instanceof Player){
						if(NumberUtils.isNumber(args[2])){
							int amt=Integer.parseInt(args[2]);
							if(args[0].equalsIgnoreCase("add")||args[0].equalsIgnoreCase("a")){
								balance.addCoins(target,amt);
								rec(prefix+"You just added "+amt+" coins to "+target.getName()+"'s account.");
							}else if(args[0].equalsIgnoreCase("subtract")||args[0].equalsIgnoreCase("s")){
								balance.subCoins(target,amt);
								rec(prefix+"You just subtracted "+amt+" coins from "+target.getName()+"'s account.");
							}else if(args[0].equalsIgnoreCase("set")||args[0].equalsIgnoreCase("s")){
								balance.setCoins(target,amt);
								rec(prefix+"You just set "+target.getName()+"'s balance to "+amt+" coins.");
						}else{
								a(prefix+"Correct usage: /coins <set/add/subtract> <player> <amt>");
							}
						}else{
							rec(prefix+"Invalid amount!");
						}
					}else{
						rec(prefix+"Offline or invalid player: "+args[1]);
					}
				}
			}else if(commandLabel.equalsIgnoreCase("leave")){
				worldhandler.leave((Player)sender,false);
				rec(prefix+"You have left the game.");
			}else if(commandLabel.equalsIgnoreCase("help")){
				//TODO
			}
			
		}
		return true;
	}
}
	private void a(String msg){
		target.sendMessage(ChatColor.translateAlternateColorCodes('&',msg));
	}
	private void rec(String msg){
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&',msg));
	}
}