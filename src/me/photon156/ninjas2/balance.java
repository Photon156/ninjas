package me.photon156.ninjas2;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class balance implements Listener{
	private final Main plugin;
	@SuppressWarnings("unused")
	private final Main Main;
	public balance(Main plugin, Main main){
		this.plugin=plugin;
		this.Main=main;
		Bukkit.getServer().getPluginManager().registerEvents(this,plugin);
	}
	public void addCoins(Player target,int amt){
		plugin.getConfig().set(String.valueOf(target.getUniqueId()+".bal"),plugin.getConfig().getInt(target.getUniqueId()+".bal")+amt);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	public void setCoins(Player target,int amt){
		plugin.getConfig().set(String.valueOf(target.getUniqueId()+".bal"),amt);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	public void subCoins(Player target,int amt){
		plugin.getConfig().set(String.valueOf(target.getUniqueId()+".bal"),plugin.getConfig().getInt(target.getUniqueId()+".bal")-amt);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
}
