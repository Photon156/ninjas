package me.photon156.ninjas2;

import org.bukkit.entity.Player;

/*
 * [Purpose]: Set config defaults for worlds' spawnpoints & other config things
 */

public class setup {
public final Main plugin;
public setup(Main plugin){
	this.plugin=plugin;
}
public void playerDefaults(Player target){
	items items=new items(plugin,target);
	if(!plugin.getConfig().contains(target.getUniqueId()+".hat")){
		plugin.getConfig().set(target.getUniqueId()+".hat",items.armor[0]);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	if(!plugin.getConfig().contains(target.getUniqueId()+".vest")){
		plugin.getConfig().set(target.getUniqueId()+".vest",items.armor[3]);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	if(!plugin.getConfig().contains(target.getUniqueId()+".pants")){
		plugin.getConfig().set(target.getUniqueId()+".pants",items.armor[6]);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	if(!plugin.getConfig().contains(target.getUniqueId()+".boots")){
		plugin.getConfig().set(target.getUniqueId()+".boots",items.armor[9]);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	if(!plugin.getConfig().contains(target.getUniqueId()+".sword")){
		plugin.getConfig().set(target.getUniqueId()+".sword",items.sword[0]);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	if(!plugin.getConfig().contains(target.getUniqueId()+".bal")){
		plugin.getConfig().set(target.getUniqueId()+".bal",200);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	if(!plugin.getConfig().contains(target.getUniqueId()+".grenades")){
		plugin.getConfig().set(target.getUniqueId()+".grenades",2);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	if(!plugin.getConfig().contains(target.getUniqueId()+".shurikens")){
		plugin.getConfig().set(target.getUniqueId()+".shurikens",5);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	if(!plugin.getConfig().contains(target.getUniqueId()+".vip")){
		plugin.getConfig().set(target.getUniqueId()+".vip",false);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
	if(!plugin.getConfig().contains(target.getUniqueId()+".kills")){
		plugin.getConfig().set(target.getUniqueId()+".kills",0);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
}
}
