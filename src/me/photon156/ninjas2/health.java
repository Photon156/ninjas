package me.photon156.ninjas2;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class health implements Listener{
private final Main plugin;
private final Main Main;
public health(Main plugin, Main main){
	this.plugin=plugin;
	this.Main=main;
	Bukkit.getServer().getPluginManager().registerEvents(this,plugin);
}
String prefix="&8[&aNinjas&8] &7"; //Ninjas plugin prefix
Player target;
@SuppressWarnings("deprecation")
@EventHandler
public void onDamage(EntityDamageByEntityEvent event){
	if(event!=null&&event.getEntity()!=null&&event.getEntity() instanceof Player){
		target=(Player)event.getEntity();
		worldhandler worldhandler=new worldhandler(plugin,Main);
		balance balance=new balance(plugin,Main);
		ninja ninja=new ninja(Main);
		if(worldhandler.inGame(target)){
			if(target.getHealth()-event.getDamage()<=0.0&&event.getDamager() instanceof Player){
				event.isCancelled();
				worldhandler.leave(target,false);
				Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&',"&8[&1Announcer&8] &7"+event.getDamager().getName()+" just killed "+target.getName()+"!"));
				ninja.pop(target);
				int amt=ninja.getVip()?40:20;
				Player damager=Bukkit.getPlayer(event.getDamager().getName());
				balance.addCoins(damager,amt);
				send(damager,prefix+"&a+"+amt+" Coins");
				balance.subCoins(target,amt/2);
				send(target,prefix+"&c-"+amt/2+" Coins");
				plugin.getConfig().set(target.getUniqueId()+".kills",plugin.getConfig().getInt(target.getUniqueId()+".kills")+1);
				plugin.saveConfig();
				plugin.reloadConfig();
			}
			/**PVPLOGGER CODE STARTS HERE*/
			Player damager=Bukkit.getPlayer(event.getDamager().getName());
			dmgCounter dmgTarget=new dmgCounter(Main);
			dmgCounter dmgDamager=new dmgCounter(Main);
			System.out.println("inCombat:");
			Main.inCombat.forEach((Player player,String string)->System.out.println(player.getName()+" "+string));//LAMBDA
			if(Main.inCombat.containsKey(target)){
				Main.threads.get(target).interrupt();
				Main.threads.remove(target);
				Main.inCombat.remove(target);
				System.out.println("contains target");
			}else{
				send(prefix+"You are now in combat!");
			}
			if(Main.inCombat.containsKey(damager)){
				Main.threads.get(damager).interrupt();
				Main.threads.remove(damager);
				Main.inCombat.remove(damager);
				System.out.println("contains damager");
			}else{
				send(damager,prefix+"You are now in combat!");
			}
			Main.inCombat.put(target,target.getName());
			Main.inCombat.put(damager,damager.getName());
			System.out.println("starting...");
			dmgTarget.begin(damager,10000);
			dmgDamager.begin(target,10000);
			if(event.getCause()==EntityDamageEvent.DamageCause.VOID){
				worldhandler.leave(target,false);
			}
		}
	}
}
public void grenadeKillPlayer(Player target,Player damager){
	worldhandler worldhandler=new worldhandler(plugin,Main);
	worldhandler.leave(target,false);
	if(target==damager){
		Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&',"&8[&1Announcer&8] &7"+target.getName()+" just blew themself up. Wow."));
		balance balance=new balance(plugin,Main);
		balance.subCoins(target,15);
		send(target,prefix+"&c-15 Coins");
	}else{
		Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&',"&8[&1Announcer&8] &7"+damager.getName()+" just killed "+target.getName()+"!"));
		ninja ninja=new ninja(Main);
		ninja.pop(target);
		int amt=ninja.getVip()?40:20;
		balance balance=new balance(plugin,Main);
		balance.addCoins(damager,amt);
		send(damager,prefix+"&a+"+amt+" Coins");
		balance.subCoins(target,amt/2);
		send(target,prefix+"&c-"+amt/2+" Coins");
		plugin.getConfig().set(target.getUniqueId()+".kills",plugin.getConfig().getInt(target.getUniqueId()+".kills")+1);
		plugin.saveConfig();
		plugin.reloadConfig();
	}
}
public boolean inCombat(Player target){
	if(Main.inCombat.containsKey(target)) return true; else return false;
}
private final void send(String message){ //[Ease] Sends messages to players
	target.sendMessage(ChatColor.translateAlternateColorCodes('&',message)); //So we can use &'s for colors
}
private final void send(Player target,String message){
	target.sendMessage(ChatColor.translateAlternateColorCodes('&',message));
}
}