package me.photon156.ninjas2;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class onJoin implements Listener{
private final Main plugin;
public onJoin(Main plugin){
	this.plugin=plugin;
	Bukkit.getServer().getPluginManager().registerEvents(this,plugin);
}
@EventHandler
public void join(PlayerJoinEvent event){
	if(event.getPlayer()!=null){
		if(!plugin.getConfig().contains(event.getPlayer().getUniqueId().toString())){
			setup setup=new setup(plugin);
			setup.playerDefaults(event.getPlayer());
		}
		if(!event.getPlayer().getInventory().contains(Material.WRITTEN_BOOK)){
			items items=new items(plugin,event.getPlayer());
			event.getPlayer().getInventory().addItem(new ItemStack(items.book));
		}
	}
}
@EventHandler
public void onDisconnect(PlayerQuitEvent event){
	if(event.getPlayer()!=null){
		worldhandler wh=new worldhandler(plugin,plugin);
		if(wh.inGame(event.getPlayer())){
			wh.leave(event.getPlayer(),false);
		}
	}
}
}