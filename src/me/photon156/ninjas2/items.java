package me.photon156.ninjas2;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/*
 * [Purpose]: Constructor sets all of the armors' types & colors
 * for use by the other classes
 */

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class items {
	@SuppressWarnings("unused")
	private final Main Main;
public items(Main Main,Player target){
	this.Main=Main;
	ItemStack[] sword={new ItemStack(Material.IRON_SWORD),new ItemStack(Material.GOLD_SWORD),new ItemStack(Material.DIAMOND_SWORD)};
	ItemStack[] armor={helmet(255,0,0),helmet(0,255,0),helmet(0,0,255),vest(255,0,0),vest(0,255,0),vest(0,0,255),leggings(255,0,0),leggings(0,255,0),leggings(0,0,255),boots(255,0,0),boots(0,255,0),boots(0,0,255)};
	ninja ninja=new ninja(Main);
	ninja.pop(target);
	ItemStack star=new ItemStack(Material.NETHER_STAR,ninja.getShurikens());
	ItemMeta s=star.getItemMeta();
	s.setDisplayName("§aShuriken");
    ArrayList<String> l = new ArrayList<String>();
    l.add(ChatColor.GRAY + "Throw your Shuriken");
    s.setLore(l);
    star.setItemMeta(s);
    ItemStack tnt=new ItemStack(Material.TNT,ninja.getGrenades());
    ItemMeta meta=tnt.getItemMeta();
    meta.setDisplayName("§4Grenade");
    ArrayList<String> l_a = new ArrayList<String>();
    l_a.add(ChatColor.GRAY + "Throw your Grenade");
    meta.setLore(l_a);
    tnt.setItemMeta(meta);
    ItemStack[] extras={star,tnt};
	this.armor=armor;
	this.sword=sword;
	this.extras=extras;
	ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta)book.getItemMeta();
    bookMeta.setTitle((Object)ChatColor.RED + "Ninjas Guidebook");
    bookMeta.setAuthor((Object)ChatColor.GRAY + "The Dragon");
    bookMeta.setPages(Arrays.asList((Object)ChatColor.GOLD + "How to Play" + (Object)ChatColor.GRAY + "\n-------------------" + (Object)ChatColor.DARK_GREEN + "\n- Use Your Grappling Hook to get around\n\n- Double tap space for a double jump\n\n- Fight Other Players to get Coins\n\n- Spend Your coins in the Shop", (Object)ChatColor.GOLD + "Tips" + (Object)ChatColor.GRAY + "\n-------------------" + (Object)ChatColor.DARK_GREEN + "\n- Sometimes you may have to grapple more than once to get where you want.\n\n- Have an escape route ready! Fire your grappling hook, then switch to your sword to fight! When your health is low, switch back to your hook ", (Object)ChatColor.GOLD + "More" + (Object)ChatColor.GRAY + "\n-------------------" + (Object)ChatColor.DARK_GREEN + "\nand grapple away!\n\nMore tips coming soon..."));
    book.setItemMeta((ItemMeta)bookMeta);
    this.book=book;
}
ItemStack book;
ItemStack[] armor;
ItemStack[] sword;
ItemStack[] extras;
private ItemStack helmet(int r,int g,int b){
	ItemStack helmet=new ItemStack(Material.LEATHER_HELMET,1);
	LeatherArmorMeta helmetMeta=(LeatherArmorMeta)helmet.getItemMeta();
	helmetMeta.setColor(Color.fromRGB(r,g,b));
	helmet.setItemMeta(helmetMeta);
	return helmet;
}
private ItemStack vest(int r,int g,int b){
	ItemStack helmet=new ItemStack(Material.LEATHER_CHESTPLATE,1);
	LeatherArmorMeta helmetMeta=(LeatherArmorMeta)helmet.getItemMeta();
	helmetMeta.setColor(Color.fromRGB(r,g,b));
	helmet.setItemMeta(helmetMeta);
	return helmet;
}
private ItemStack leggings(int r,int g,int b){
	ItemStack helmet=new ItemStack(Material.LEATHER_LEGGINGS,1);
	LeatherArmorMeta helmetMeta=(LeatherArmorMeta)helmet.getItemMeta();
	helmetMeta.setColor(Color.fromRGB(r,g,b));
	helmet.setItemMeta(helmetMeta);
	return helmet;
}
private ItemStack boots(int r,int g,int b){
	ItemStack helmet=new ItemStack(Material.LEATHER_BOOTS,1);
	LeatherArmorMeta helmetMeta=(LeatherArmorMeta)helmet.getItemMeta();
	helmetMeta.setColor(Color.fromRGB(r,g,b));
	helmet.setItemMeta(helmetMeta);
	return helmet;
}
}
