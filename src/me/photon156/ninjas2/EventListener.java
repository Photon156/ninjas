package me.photon156.ninjas2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class EventListener implements Listener {
	private final Main plugin;
	private final Main Main;
	public EventListener(Main plugin, Main main){
		this.plugin=plugin;
		this.Main=main;
		Bukkit.getServer().getPluginManager().registerEvents(this,plugin);
		// This is the that handles a shuriken being midair
				// Constantly running
				Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
				    @Override
				    public void run() {
				    	//removeFromList();
				    	
				    	// This iterates through all the shurikens in the reference list
				    	// First it checks if the shuriken is motionless on the ground
				    	// If so, it removes it
				    	// Otherwise it checks each shuriken to see if it is very close to an entity
				    	// If so, damage the entity
				    	// If not, do nothing and move on
				    	for (Item i : itemPlayer.keySet()) {
				    		if (i != null) {
				    		//sets key for item
				    		Object key = i;
				    		//Check if on ground
				    		if ((i.isOnGround()) || (i.getVelocity().equals(0.0D))) {
				    			toRemoveFromList.add(i);
				    			i.remove();
								itemPlayer.remove(key);
				    			return;
							}
				    		
				    		//Check nearby players for the shuriken
				    		for (Entity entity: i.getNearbyEntities(0.3, 0.3, 0.3)) {
				    			if (entity.getType() == EntityType.PLAYER) {
				    				if (entity != itemPlayer.get(key)) {
				    					if (((Player) entity).getGameMode() != GameMode.CREATIVE) {
				    						Damageable player = (Player) entity;
					    					
					    					Entity damager = itemPlayer.get(i);
					    					
					    					Player damager2 = itemPlayer.get(i);
					    					
					    					Double hp = player.getHealth();
						    				
							    			player.damage(2.0, damager);
							    			
							    			if (player.getHealth() == hp){
							    				if (player.getHealth() - 3.0 < 0) {
							    					player.setHealth(0.1);
							    					player.damage(0.1);
							    				}
							    				if (!(player.getHealth() - 3.0 < 0)) {
							    					player.setHealth(player.getHealth() - 3.0);
							    					player.damage(0.0);
							    				}
							    			}
							    			
							    			// Plays a 'hit' note to signify the shuriken hit
							    			damager2.playNote(damager2.getLocation(), Instrument.PIANO, new Note(1, Note.Tone.G, true));
											
							    			toRemoveFromList.add(i);
							    			
											i.remove();
											
											return;
				    					}
				    				}
				    				}
				    			}
				    		}
				    	}
				    } 
				}, 1L, 1L);
	}
	Map<Item, Player> itemPlayer = new HashMap<Item, Player>(); // The shuriken to player references
	List<Item> toRemoveFromList = new ArrayList<Item>(); // uuhh?? forgot what this did 
	Map<Item, Player> grenadePlayer = new HashMap<Item, Player>(); // The grenade to player reference
	Map<String, Location> shotGrapple = new HashMap<String, Location>(); // The location saved from a connected grapple shot

	// THIS
	// Is a list of players that are CURRENTLY in the game/arena
	// You need to change this somehow to contain all the players that are currently in an arena
	// \/
	List<Player> players = new ArrayList<Player>();
	@SuppressWarnings("deprecation")
	@EventHandler
	  public void onPlayerInteract(PlayerInteractEvent event){
		worldhandler wh=new worldhandler(Main,plugin);
		  if (wh.inGame(event.getPlayer())){
	    if ((event.getAction() == Action.RIGHT_CLICK_AIR) || (event.getAction() == Action.RIGHT_CLICK_BLOCK)){ // If the player right clicks
	      if (event.getPlayer().getInventory().getItemInHand().getType() == Material.LEASH){ // Checks of the player is holding a leash
	        if (this.shotGrapple.get(event.getPlayer().getName()) == null) { // Checks to see if a player's arrow has not already hit
	          Arrow arrow = (Arrow)event.getPlayer().launchProjectile(Arrow.class); // Creates an arrow
	          Vector velo = event.getPlayer().getLocation().getDirection(); // Gets direction vector of player's direction
	          velo.multiply(3); // Adds a velocity to that vector (speed)
	          arrow.setVelocity(velo); // Gives the arrows that speed
	        } else if (this.shotGrapple.get(event.getPlayer().getName()) != null) { // If the player's arrow HAS connected with a block
	          Player player = event.getPlayer(); // Gets player information \/
	          Location lc = player.getLocation();
	          Location to = (Location)this.shotGrapple.get(player.getName()); // Gets the location where the arrow hit
	          lc.setY(lc.getY() + 0.5D); // Prepares to move the players location up half a block because of a glitch
	          player.teleport(lc); // Moves the player up
	          this.shotGrapple.remove(player.getName()); // Gets rid of the item in the list of connected grapples
	          
	          //XXX Some crazy math to calculate how to throw the player that i grabbed from online
	          //XXX Up to change because it's not always great at predicting the right velocity
	          double g = -0.08D;
	          double d = to.distance(lc);
	          double t = d;
	          double v_x = (1.0D + 0.07000000000000001D * t) * (to.getX() - lc.getX()) / t;
	          double v_y = (1.0D + 0.03D * t) * (to.getY() - lc.getY()) / t - 0.5D * g * t;
	          double v_z = (1.0D + 0.07000000000000001D * t) * (to.getZ() - lc.getZ()) / t;

	          // Continued..
	          Vector v = event.getPlayer().getVelocity();
	          v.setX(v_x);
	          v.setY(v_y);
	          v.setZ(v_z);
	          event.getPlayer().setVelocity(v); // Sets the correct velocity for player
	          // Keeps player from dying, this gets reset when they hit the ground
	          player.setFallDistance(10000000.0F);
	        }

	      }

	      /** THIS is the code I used to open up the shop and its GUI, disregard for a while until we take on GUIs
	      if ((event.getPlayer().getInventory().getItemInHand().getType() == Material.EMERALD) && 
	        (!this.players.contains(event.getPlayer())))
	      {
	        Player p = event.getPlayer();

	        Inventory inv = p.getPlayer().getServer().createInventory(null, 9, "Shop");

	        ItemStack istack = new ItemStack(Material.IRON_INGOT);
	        ItemMeta imeta = istack.getItemMeta();
	        imeta.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Upgrade Sword");
	        ArrayList<String> lore = new ArrayList<String>();
	        lore.add(ChatColor.GRAY + "Enter Sword");
	        lore.add(ChatColor.GRAY + "Upgrade Menu");
	        imeta.setLore(lore);
	        istack.setItemMeta(imeta);
	        inv.setItem(2, istack);

	        ItemStack istack2 = new ItemStack(Material.NETHER_STAR);
	        ItemMeta imeta2 = istack2.getItemMeta();
	        imeta2.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Items");
	        ArrayList<String> lore2 = new ArrayList<String>();
	        lore2.add(ChatColor.GRAY + "Enter Items");
	        lore2.add(ChatColor.GRAY + "Menu");
	        imeta2.setLore(lore2);
	        istack2.setItemMeta(imeta2);
	        inv.setItem(4, istack2);
	        
	        ItemStack istack3 = new ItemStack(Material.LEATHER_CHESTPLATE);
	        ItemMeta imeta3 = istack3.getItemMeta();
	        imeta3.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Armor");
	        ArrayList<String> lore3 = new ArrayList<String>();
	        lore3.add(ChatColor.GRAY + "Enter Armor");
	        lore3.add(ChatColor.GRAY + "Menu");
	        imeta3.setLore(lore3);
	        istack3.setItemMeta(imeta3);
	        inv.setItem(6, istack3);

	        p.getPlayer().openInventory(inv);
	      }

	    } */

	    if (((event.getAction() == Action.LEFT_CLICK_AIR) || (event.getAction() == Action.LEFT_CLICK_BLOCK)) && (event.getPlayer().getInventory().getItemInHand().getType() == Material.LEASH)) { // Gets if player left clicked with leash
	      // All this stuff just removes the grapple for the player
	      Player player = event.getPlayer();
	      if (this.shotGrapple.get(event.getPlayer().getName()) != null) {
	        this.shotGrapple.remove(player.getName());
	        player.sendMessage("�8[�aNinjas�8] �7Grapple Removed!");
	      }
	    }
	    
	    //Throw Shuriken
	    Player player = event.getPlayer();
	    int blockId = player.getItemInHand().getType().getId();
	    if (blockId == 399 && event.getAction() == Action.RIGHT_CLICK_AIR || blockId == 399 && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
	        // If player right clicks with a shuriken
	    	event.setCancelled(true); // This keeps anything from actually happening
	    	// e.g. if you cancelled a right click event of a snowball, it wouldn't throw
	    	// Slightly unnecessary here but it's a good habit
	    	
	    	// Drops a itemstack of a nether star into the world and throws it in the direction the player is looking
	    	Item item = player.getWorld().dropItem(player.getEyeLocation(), new ItemStack(Material.NETHER_STAR));
	        item.setVelocity(player.getLocation().getDirection().multiply(2.2D));
	        item.setPickupDelay(Integer.MAX_VALUE);
	        
	        //FIXME This gets the item to remove from your inventory
	        //FIXME THIS MIGHT NEED TO BE ADJUSTED
	        //FIXME If the nether stars are not being removed
	        //FIXME Probably because of the lore information etc.
	        ItemStack removeitem = new ItemStack(Material.NETHER_STAR, 1);
	        ItemMeta meta = removeitem.getItemMeta();
	        meta.setDisplayName("�aShuriken");
	        ArrayList<String> l = new ArrayList<String>();
	        l.add(ChatColor.GRAY + "Throw your Shuriken");
	        meta.setLore(l);
	        removeitem.setItemMeta(meta);
	        //Remove Item
	        player.getInventory().removeItem(removeitem);
	        player.updateInventory();
	        item.getWorld().playSound(item.getLocation(), Sound.STEP_SNOW, 5, 5);
	        
	        // Puts item in itemPlayer Map, to reference by runnable
	        // Which means that the nether star that was thrown has a reference now
	        itemPlayer.put(item, player);
	    }
	    
	    /** TNT CODE ===============================================================================================*/
	    if (blockId == 46 && event.getAction() == Action.RIGHT_CLICK_AIR || blockId == 46 && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
	    	if (wh.inGame(player)) {
		    	// Similar in many way to the Nether star code
		    	event.setCancelled(true);
		    	final Item item = player.getWorld().dropItem(player.getEyeLocation(), new ItemStack(Material.TNT));
		        item.setVelocity(player.getLocation().getDirection().multiply(1.05D));
		        item.setPickupDelay(Integer.MAX_VALUE);
		        //Get Item to remove
		        ItemStack removeitem = new ItemStack(Material.TNT, 1);
		        ItemMeta meta = removeitem.getItemMeta();
		        meta.setDisplayName("�4Grenade");
		        ArrayList<String> l = new ArrayList<String>();
		        l.add(ChatColor.GRAY + "Throw your Grenade");
		        meta.setLore(l);
		        removeitem.setItemMeta(meta);
		        //Remove Item
		        player.getInventory().removeItem(removeitem);
		        player.updateInventory();
		        item.getWorld().playSound(item.getLocation(), Sound.STEP_SNOW, 5, 5);
		        //Puts item in itemPlayer Map, to reference by runnable
		        grenadePlayer.put(item, event.getPlayer());
		        // This runnable is a countdown for the tnt object basically
		        // After the set time, if damages the entities near the item depending on how close they are to it
		        //
		        //XXX NOTE: this uses the 'grenadeKillPlayer' method, which basically
		        // just manually kills a player in order to get the correct
		        // death message
		        new BukkitRunnable() {
		        	@Override
		            public void run() {
		              for(Entity nearbyEntity : item.getNearbyEntities(5D, 5D, 5D)) {
		                if(nearbyEntity instanceof Player) {
		                	Player pl = (Player) nearbyEntity;
		                	Player damager = grenadePlayer.get(item);
		                	Damageable p = (Damageable) pl;
							double distance = item.getLocation().distance(pl.getLocation());
							double damageFactor = 3;
							double totalDamage = 0.0;
							//Calculate damage
							if ((6.0 - distance) > 0.0) {
								totalDamage = (6.0 - distance) * damageFactor;
							}
							
							if ((p.getHealth() - totalDamage) <= 0.0) {
								health health=new health(Main,plugin);
								health.grenadeKillPlayer(pl, damager);
							} else {
								p.damage(totalDamage);
							}
			                item.getWorld().createExplosion(item.getLocation().getX(), item.getLocation().getY(), item.getLocation().getZ(), 2.0F, false, false);
			                item.remove();
			                grenadePlayer.remove(item);
			                return;
		                }
		                item.getWorld().createExplosion(item.getLocation().getX(), item.getLocation().getY(), item.getLocation().getZ(), 2.0F, false, false);
		                item.remove();
		                grenadePlayer.remove(item);
		                return;
		              }
		        	}
		        }.runTaskLater(plugin,60L);
	    	}
	    }
	  }
	 }
	
	  }
}